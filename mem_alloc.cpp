extern "C" void *my_malloc(unsigned int size);
extern "C" int my_free(void *address);
extern "C" {
	#include <stdio.h>
	//#include <stdlib.h>
	//#include "types.h"
};

typedef char ALIGN[16];



union header {
	struct {
		size_t size;
		unsigned is_free;
		union header *next;
	} s;
	ALIGN stub;
};
typedef union header header_t;

//Boilerplate primo z hello.c
unsigned syscall2(unsigned w0, unsigned w1) {
    asm volatile (
        "   mov %%esp, %%ecx    ;"
        "   mov $1f, %%edx      ;"
        "   sysenter            ;"
        "1:                     ;"
        : "+a" (w0) : "S" (w1) : "ecx", "edx", "memory");
    return w0;
};

void *brk(void *address) {
    return (void*)syscall2(3, (unsigned)address);
};

header_t *head, *tail;

header_t *get_free_block(size_t size) {
	header_t *curr = head;

	while(curr) {
		if (curr->s.is_free && curr->s.size >= size)
			return curr;
		curr = curr->s.next;
	};
	return NULL;
};

void *my_malloc(size_t size)
{
	size_t total_size;
	void *block;
	header_t *header;

	if (!size) return NULL;
	header = get_free_block(size);

	if (header) {
		header->s.is_free = 0;
		return (void*)(header + 1);
	};
	total_size = sizeof(header_t) + size;
	void *curr_break = brk(0);
	block = brk((void*)(total_size + (size_t)curr_break));

	if (block == (void*) -1 || block == 0) {
		return NULL;
	};
	header = (header_t*)block;
	header->s.size = size;
	header->s.is_free = 0;
	header->s.next = NULL;

	if (!head) head = header;

	if (tail) tail->s.next = header;
	tail = header;
	return (void*)(header + 1);
};

int my_free(void *block) {
	header_t *header, *tmp;
	void *programbreak;

	if (!block) return -1;
	if(head == NULL || tail == NULL) return -1;
	if(block < head || block > tail + sizeof(header_t)) return -1;

	header = (header_t*)block - 1;
	programbreak = brk(0);
	
	if ((char*)block + header->s.size == programbreak) {
		if (head == tail) {
			head = tail = NULL;
		} else {
			tmp = head;
			while (tmp) {
				if(tmp->s.next == tail) {
					tmp->s.next = NULL;
					tail = tmp;
				};
				tmp = tmp->s.next;
			};
		};
		brk((void *)((size_t)programbreak - sizeof(header_t) - header->s.size));
		//
		return 0;
	};
	//TODO: check if there are any blocks eligible for merging
	header->s.is_free = 1;
	return 0;
};