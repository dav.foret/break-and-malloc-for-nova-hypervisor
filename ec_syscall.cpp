#include "ec.h"
#include "ptab.h"
#include "stdio.h"
#include "string.h"
#include "bits.h"

typedef enum {
    sys_print      = 1,
    sys_sum        = 2,
    sys_break      = 3,
    sys_thr_create = 4,
    sys_thr_yield  = 5,
} Syscall_numbers;

void Ec::syscall_handler (uint8 a)
{
    // Get access to registers stored during entering the system - see
    // entry_sysenter in entry.S
    Sys_regs * r = current->sys_regs();
    Syscall_numbers number = static_cast<Syscall_numbers> (a);

    switch (number) {
        case sys_print: {
            char *data = reinterpret_cast<char*>(r->esi);
            unsigned len = r->edi;
            for (unsigned i = 0; i < len; i++)
                printf("%c", data[i]);
            break;
        }
        case sys_sum: {
            // Naprosto nepotřebné systémové volání na sečtení dvou čísel
            int first_number = r->esi;
            int second_number = r->edi;
            r->eax = first_number + second_number;
            break;
        }
        case sys_break: {
            //Set-up current & requested program breaks & limits
            mword req_brk = r->esi;
            mword curr_brk = Ec::break_current;
            mword max_brk = 0xC0000000;
            //Special case: return current program break
            if(req_brk == 0 || req_brk == curr_brk){
                r->eax = curr_brk;
                ret_user_sysexit();
            };
            //Break not allowed
            if(req_brk < current->break_min || req_brk > max_brk) {
                r->eax = 0;
                ret_user_sysexit();
            };  
            
            mword curr_page = align_dn(curr_brk - 1, PAGE_SIZE);
            mword req_page = align_dn(req_brk - 1, PAGE_SIZE);
            mword tmp_page;
            //Allocation required
            if(req_brk > curr_brk) {
                unsigned err = 0;

                for(tmp_page = curr_page + PAGE_SIZE; tmp_page <= req_page && !err; tmp_page += PAGE_SIZE) {
                    void* new_page = Kalloc::allocator.alloc_page(1, Kalloc::FILL_0);
                    mword new_frame = Kalloc::allocator.virt2phys(new_page);

                    if(!new_page) {
                        err = 1;
                        break;
                    };

                    if(!Ptab::insert_mapping(tmp_page, new_frame, Ptab::PRESENT | Ptab::RW | Ptab::USER)) {
                        err = 1;
                        break;
                    };
                };
                //Mapping failed at page no. err, de-alloc everything up to curr_page
                if(err) {
                    for(tmp_page -= PAGE_SIZE; tmp_page > curr_page; tmp_page -= PAGE_SIZE) {
                        void *page = Kalloc::allocator.phys2virt(Ptab::get_mapping(tmp_page) & ~PAGE_MASK);
                        Ptab::insert_mapping(tmp_page, 0, 0);
                        Kalloc::allocator.free_page(page);
                    };
                    r->eax = 0;
                    ret_user_sysexit();
                } 
                //Zero required parts of current page
                mword mem_size = (req_page > curr_page) ? (align_up(curr_brk - 1, PAGE_SIZE) - curr_brk) : (req_brk - curr_brk);
                memset(reinterpret_cast<void *>(curr_brk), 0, mem_size);

                Ec::break_current = req_brk;
                r->eax = curr_brk;
                ret_user_sysexit();

            } else {
                //De-allocating
                for(tmp_page = curr_page; tmp_page > req_page; tmp_page -= PAGE_SIZE) {
                    void *page = Kalloc::allocator.phys2virt(Ptab::get_mapping(tmp_page) & ~PAGE_MASK);
                    Ptab::insert_mapping(tmp_page, 0, 0);
                    Kalloc::allocator.free_page(page);
                };
                Ec::break_current = req_brk;
                r->eax = curr_brk;
                ret_user_sysexit();
            };
            break;
        }  
        default: {
            printf ("unknown syscall %d\n", number);
            break;
        };
    };
    ret_user_sysexit();
}
